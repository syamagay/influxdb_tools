#!/usr/bin/env python3

from influxdb import InfluxDBClient
from datetime import datetime
import linecache


def main():
    temperature_1 = float(linecache.getline('test.txt', 2).split(' ')[16].split('°')[0])
    linecache.clearcache() 
    dbname='dcsDB'
    client=InfluxDBClient(host='localhost',
                          port=8086,
                          database=dbname);
    list_database=client.get_list_database()
    if not {"name" : dbname } in list_database :
        client.create_database(dbname)
    else :
        pass
        #print('database : {} is already exist.'.format(dbname))

    mes_name='dummy_thermal'
        
    temperature_unit='[C]'
    now=datetime.utcnow()
    str_now=datetime.strftime(now,'%Y-%m-%dT%H:%M:%SZ')
    data=[{'fields' : { 'temperature_1' : temperature_1},
           'measurement' : mes_name,
           #'time':datetime.utcnow(),
           'time':str_now,
           'tags':{'institute':'Pallet Town',
                   'temperature_1_unit':temperature_unit}
       }]
    res=client.write_points(data)

if __name__=='__main__': main()
