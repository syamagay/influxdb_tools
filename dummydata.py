#!/usr/bin/env python3

from influxdb import InfluxDBClient
from datetime import datetime
import random
import argparse
import time

default_count=1000

def getArgs():
    parser=argparse.ArgumentParser(description='data downloader from InfluxDB')
    parser.add_argument('--count', help='provided count (default:'+str(default_count)+')')
    args=parser.parse_args()
    return args 

def main(count_max):
    dbname='dcsDB'
    client=InfluxDBClient(host='localhost',
                          port=8086,
                          database=dbname);
    list_database=client.get_list_database()
    if not {"name" : dbname } in list_database :
        client.create_database(dbname)
    else :
        print('database : {} is already exist.'.format(dbname))

    mes_name='dummy_thermal'
    count=0
    while count < int(count_max) :
        temperature_1=random.uniform(22.0,28.0)
        temperature_2=random.uniform(22.0,28.0)
        temperature_3=random.uniform(22.0,28.0)
        setting_temp_1=28
        setting_temp_2=28
        setting_temp_3=28
        
        temperature_unit='[C]'
        now=datetime.utcnow()
        str_now=datetime.strftime(now,'%Y-%m-%dT%H:%M:%SZ')
        data=[{'fields' : { 'temperature_1' : temperature_1,
                            'temperature_2' : temperature_2,
                            'temperature_3' : temperature_3},
               'measurement' : mes_name,
               #'time':datetime.utcnow(),
               'time':str_now,
               'tags':{'institute':'Pallet Town',
                       'temperature_1_unit':temperature_unit,
                       'temperature_2_unit':temperature_unit,
                       'temperature_3_unit':temperature_unit}
           }]
        res=client.write_points(data)
        count+=1
        time.sleep(2)

if __name__=='__main__':
    args=getArgs()
    if args.count is None:
        count=default_count
    else:
        count=args.count
    main(count)
    
